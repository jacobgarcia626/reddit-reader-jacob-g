package com.example.student.redditreader;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class RedditListFragment extends Fragment {

    String[] redditPosts = new String[] {
        "Spot my mixtape?",
        "ey b0ss",
        "( ͡° ͜ʖ ͡°)",
        "John Cena"
    };


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
         View view = inflater.inflate(R.layout.fragment_reddit_list, container, false);

        RecyclerView recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        new RedditPostTask().execute(this);

        RedditPostAdapter adapter = new RedditPostAdapter(redditPosts);
        recyclerView.setAdapter(adapter);

        return view;
    }
}
